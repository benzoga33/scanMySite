#!/usr/bin/perl
#*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
#
#	////////////////////////////////////
#	 	     T@Ll3yR4nD
#	////////////////////////////////////
#
#	Title : ScanMySite
#	Category : Remote
#	
#*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

use HTTP::Request;
use LWP::UserAgent;
use strict;
use warnings;

#prerequis 
if ($#ARGV != 0) {
	    print "usage: scanMySite.pl liste.txt\n";
	    exit;
}
my $filename=$ARGV[0];
my $log="scanMySite.log";
#purge du log
if (-e $log){
	unlink $log;
}
open (LOG, ">$log") || die ("Impossible de créer le log\n");
#IHM de saisie nom du site 
print "\t\t_________________________________________________\n\n";
print "\t\t\t ScanMySite v.1.0\n";
print "\t\t_________________________________________________\n";
print "\n\n";
sleep (1);
    print "\n\n";
    print "\t HOST=> (ex: http://www.mysite.com)\n";
    print "\t HOST=> :";
    my $host=<STDIN>;
    chomp($host);
    if($host !~ /http:\/\//) { $host = "http://$host/"; };
print "\n\n";
print "\t\t*-*-*-*-*-* Scanning *-*-*-*-*-*\n";
print "\n\n";

open(FIC , "<$filename") || die ("Impossible de lire le fichier\n");
#traitement du fichier liste
while (<FIC>){
	my $url=$host.$_;

	my $request = HTTP::Request->new(GET=>$url);
	my $useragent = LWP::UserAgent->new();

	my $response = $useragent->request($request);
	if ($response->is_success){print "Found : $url\n";print LOG "Found : $url\n";}
	if ($response->content=~ /Access Denied/){print "Found : $url =>[Error & Access Denied]\n";print LOG "Found : $url =>[Error & Access Denied]\n";}
	else {
		print "NotFound : $_\n";
	}
}
close (FIC);
close (LOG);
print "##########################################################\n";

print "Les résultats du scan sont disponibles:\n";
print "dans le fichier : $log\n";

print "##########################################################\n";
